package com.example.carleanesousa.myapplication;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
      TextView Ano;
      TextView Meses;
      EditText AnoCachorro;
      EditText MesesCachorro;
      Button btn ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setRequestedOrientation((ActivityInfo.SCREEN_ORIENTATION_PORTRAIT));

        Ano = (TextView)findViewById(R.id.textView2);
        Meses = (TextView)findViewById(R.id.textView3);

        AnoCachorro = (EditText)findViewById(R.id.editText);
        MesesCachorro = (EditText)findViewById(R.id.editText2);

         btn = (Button) findViewById(R.id.button);

       btn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               String textoAno = AnoCachorro.getText().toString();
               String textoMeses = MesesCachorro.getText().toString();

               double AnoCachorroo = Double.parseDouble(textoAno);
               double MesesCachorroo = Double.parseDouble(textoMeses);

               double Resultado = (AnoCachorroo / MesesCachorroo ) * 12;

                if(AnoCachorroo < 1 ){
                    Resultado = (MesesCachorroo * 1.25);

                }else if(AnoCachorroo == 1){
                    Resultado =( 15 + MesesCachorroo * 0.75);

                }else  if(AnoCachorroo == 2 && MesesCachorroo ==0){
                    Resultado = 24;

                }else if( AnoCachorroo == 2 && MesesCachorroo != 0){
                    Resultado = 24 + MesesCachorroo * 0.58;

                }else if(AnoCachorroo > 2){
                    Resultado = (AnoCachorroo - 2 + MesesCachorroo / 12) * 7 + 24;

                }

               Toast.makeText(getApplicationContext(),
                  "Idade humana do cachorro é:" + Resultado + "  Anos ",
                       Toast.LENGTH_LONG).show();

           }
       });

       }



    }

